# database_recorder

Kafka connectors that record the streams in a time series database.

Specifically, the time series database will be used to record all of our necessary data (such as random and ADL data from iot-1, iot-2 and analytics devices, and similar signal spans using DTW). These values are sent to their designated topics and then, Kakfa Connectors are used in order to send those data to the database.

The time series database that will be used for this project is going to be TimescaleDB.

Reasons for this pick:

-Though not exactly a database, Timescale is a time-series extension for PostgreSQL. I have worked with PostgreSQL in the past and I was generally satisfied with it. I also want to believe this will save me some time during setup.

-Most of the tutorials for connecting Kafka with a time-series DB were with Timescale.

-In order to send Kafka streams to any type of DB the Kafka Connect API will most likely have to be used. This API supports quite a few databases and Postgres is thankfullly among those.

Details about the installation of Timescale DB and Kafka Connect can be seen at KafkaConnect-Timescale-Installation.txt

The detailed list of the connectors used can be seen at the db_helper_files folder.
