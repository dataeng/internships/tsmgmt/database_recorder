This is a text file which keeps some of the important queries we need to use in order to retrieve some values from the Timescale database. 

This file focuses on the following tables: temp_01 (values from iot-1), ph_01 (values from iot-2) and analytics_01 (values from analytics).  

#Query 1: Retrieve calculation from analytics using the timestamp of a temperature/ph value. (Example note: The date '2021-08-31 11:14:40+00' used here is a timestamp of a temp_01.temperature value)
SELECT calculation FROM analytics_01 WHERE ABS(EXTRACT(EPOCH FROM time - '2021-08-31 11:14:40+00'::timestamptz)) <= 2.5;

#Query 2: Retrieve temperature and ph from temp_01 and ph_01 respectively, using the timestamp of an analytics calculation value.
(Example note: The date '2021-08-31 11:14:42+00' used here is a timestamp of an analytics.calculation value)
SELECT temp_01.temperature, ph_01.ph FROM temp_01, ph_01 WHERE ABS(EXTRACT(EPOCH FROM temp_01.time - '2021-08-31 11:14:42'::timestamptz)) <= 2.5 AND ABS(EXTRACT(EPOCH FROM ph_01.time - '2021-08-31 11:14:42'::timestamptz)) <= 2.5;

#Query 3: Retrieve calculations from analytics by every temperature of iot-1 (Use a similar query for every ph of iot-2)
SELECT analytics_01.calculation FROM analytics_01 INNER JOIN temp_01 ON temp_01.temperature = analytics_01.temperature; 

#Query 4: Retrieve temperature and ph from their respective tables by every calculation value of the analytics table 
SELECT temp_01.temperature, ph_01.ph FROM temp_01 INNER JOIN analytics_01 ON temp_01.temperature = analytics_01.temperature INNER JOIN ph_01 ON ph_01.ph = analytics_01.ph;

#Query 5: Retrieve all temperature values of iot-1 from a minute's pass
SELECT DATE_TRUNC('seconds',time), temperature FROM temp_01 WHERE time >= '2021-08-31 11:13:05' AND time <= '2021-08-31 11:13:30' ORDER BY 1 ASC;

Some other things to remember: Data type of the values are: numeric(4,2) for temperature and pH and numeric(5,2) for the calculation in analytics. 

Timestamp's type is: timestamptz(0). This removes the milliseconds of the timestamp because we don't really need them.

Of course, there are more tables with various different schemas. This files simply focuses on three tables of our database and the queries we can use in order to retrieve some notable values.

